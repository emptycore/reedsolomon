#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ecc.h"


unsigned char codeword[256]; //максимальная длина кодового слова
unsigned char msg[256-NPAR]; //сообщение макс длина минус четность
int erasures[256]; //для хранения пропусков
int nerasures;

int n;


void writehex(unsigned char* out, int n) { //вывод хекс-сообщения в файл и на экран
    FILE *fenc;
    fenc=fopen("encoded.txt","w");
    int i;
    for (i=0; i<n; ++i) {
        fprintf(fenc, "%02X", out[i]);
        fprintf(stdout, "%02X", out[i]);
    }
    fprintf(fenc, "\n");
    fprintf(stdout,"\n");
    fclose(fenc);
}

int main(int argc, char* argv[]) {
    fprintf(stdout, "Number of parity bytes= %d\n", NPAR);
    FILE *fdec=fopen("text.txt", "r");
    if(fdec == NULL) {
      perror("Error opening file");
      return(-1);
   }

    initialize_ecc ();

    int i;

        char* ret = fgets((char*)msg, sizeof(msg), fdec);
        const unsigned char* eos = (unsigned char*) strchr((char*)msg, NULL); //обрыв по получению первого NULL в стриме файла
        if (eos) n = eos - msg; else n=sizeof(msg)-1;
        encode_data(msg, n, codeword); //вызов encode_data из rs.c

        writehex(codeword, n+NPAR);
        fflush(stdout);

        fclose(fdec);
        scanf("%c");

    return 0;
}
