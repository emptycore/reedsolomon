#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ecc.h"


unsigned char codeword[256];
unsigned char msg[256-NPAR];
int erasures[256];
int nerasures;

int n;

int readhex(unsigned char* out, int maxn) {
    FILE *fenc;
    fenc=fopen("encoded.txt","r");
    if(fenc == NULL) {
      perror("Error opening file");
      return(-1);
   }
    char buf[516];
    int i;
    char* ret = fgets(buf, maxn*2, fenc);
    if (!ret) return 0;

    const char* eos = strchr(buf, '\n');
    if (eos) n = (eos - buf)/2; else n=maxn;

    nerasures = 0;

    for (i=0; i<n; ++i) {
        char tmp = buf[i*2+2];
        buf[i*2+2]=0;
        if(buf[i*2]=='_') {
            erasures[nerasures++]=i;
            out[i] = 0;
        } else {
            out[i] = strtol(buf+i*2, NULL, 16);
        }
        buf[i*2+2]=tmp;
    }
    fclose(fenc);
    return n;
}


int main(int argc, char* argv[]) {

    fprintf(stdout, "Number of parity bytes =%d\n", NPAR);

    initialize_ecc ();

    int i;

            n = readhex(codeword, sizeof(codeword)) - NPAR;
            if (n==-NPAR) return 0;
            for(i=0; i<nerasures; ++i) {
                erasures[i] = n+NPAR - erasures[i]-1;
            }
            decode_data(codeword, n+NPAR);
            int s = check_syndrome();
            if (s) {
                int r = correct_errors_erasures(codeword, n+NPAR, nerasures, erasures);
                if (r) {
                    fprintf(stdout, "CORRECTED: ");
                } else {
                    fprintf(stdout, "BAD: ");
                }
            } else {
                fprintf(stdout, "GOOD: ");
            }

                fwrite(codeword, 1, n, stdout);
                fprintf(stdout, "\n");

            fflush(stdout);

        scanf("%c");

    return 0;
}
