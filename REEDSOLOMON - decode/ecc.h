/****************************************************************

  НИЖЕ задается NPARD, который отвечает за коллличество байтов четности
  Ничего остальное лучше не трогать
  Чем больше байтов - тем меньше скорость работы
  Максимальная длина кодовога слова ограничена 255

  ****************************************************************/

#define NPAR 4

/****************************************************************/


#define TRUE 1
#define FALSE 0

typedef unsigned long BIT32;
typedef unsigned short BIT16;


#define MAXDEG (NPAR*2)

extern int pBytes[MAXDEG];

extern int synBytes[MAXDEG];

extern int DEBUG;

void initialize_ecc (void);
int check_syndrome (void);
void decode_data (unsigned char data[], int nbytes);
void encode_data (unsigned char msg[], int nbytes, unsigned char dst[]);

BIT16 crc_ccitt(unsigned char *msg, int len);

extern int gexp[];
extern int glog[];

void init_galois_tables (void);
int ginv(int elt);
int gmult(int a, int b);


int correct_errors_erasures (unsigned char codeword[], int csize,int nerasures, int erasures[]);

void add_polys(int dst[], int src[]) ;
void scale_poly(int k, int poly[]);
void mult_polys(int dst[], int p1[], int p2[]);

void copy_poly(int dst[], int src[]);
void zero_poly(int poly[]);
