#include <stdio.h>
#include <ctype.h>
#include "ecc.h"

/* байты четности */
int pBytes[MAXDEG];

/* синдромные байты */
int synBytes[MAXDEG];

/* генератор полиноминал */
int genPoly[MAXDEG*2];

int DEBUG = FALSE;

static void
compute_genpoly (int nbytes, int genpoly[]);

void
initialize_ecc ()
{
    init_galois_tables();

    /* Вычислить  */
    compute_genpoly(NPAR, genPoly);
}

void
zero_fill_from (unsigned char buf[], int from, int to)
{
  int i;
  for (i = from; i < to; i++) buf[i] = 0;
}

void
print_parity (void)
{
  int i;
  printf("Parity Bytes: ");
  for (i = 0; i < NPAR; i++)
    printf("[%d]:%x, ",i,pBytes[i]);
  printf("\n");
}

void
print_syndrome (void)
{
  int i;
  printf("Syndrome Bytes: ");
  for (i = 0; i < NPAR; i++)
    printf("[%d]:%x, ",i,synBytes[i]);
  printf("\n");
}

/* Добавить биты четности к концу сообщения */
void
build_codeword (unsigned char msg[], int nbytes, unsigned char dst[])
{
  int i;

  for (i = 0; i < nbytes; i++) dst[i] = msg[i];

  for (i = 0; i < NPAR; i++) {
    dst[i+nbytes] = pBytes[NPAR-1-i];
  }
}

/**********************************************************
 * Декодер
 *
 * Вычисления синдрома кодового слова. Результат в
 * synBytes[] массив.
 */

void
decode_data(unsigned char data[], int nbytes)
{
  int i, j, sum;
  for (j = 0; j < NPAR;  j++) {
    sum	= 0;
    for (i = 0; i < nbytes; i++) {
      sum = data[i] ^ gmult(gexp[j+1], sum);
    }
    synBytes[j]  = sum;
  }
}

/* Если синдром байты равны нулю*/
int
check_syndrome (void)
{
 int i, nz = 0;
 for (i =0 ; i < NPAR; i++) {
  if (synBytes[i] != 0) {
      nz = 1;
      break;
  }
 }
 return nz;
}

void
debug_check_syndrome (void)
{
  int i;

  for (i = 0; i < 3; i++) {
    printf(" inv log S[%d]/S[%d] = %d\n", i, i+1,
	   glog[gmult(synBytes[i], ginv(synBytes[i+1]))]);
  }
}


/* Генерирующий многочлен
 * Коэфициенты возвращаются genPoly аргументы
 */

static void
compute_genpoly (int nbytes, int genpoly[])
{
  int i, tp[256], tp1[256];

  /* умножить (x + a^n) for n = 1 на nbytes */

  zero_poly(tp1);
  tp1[0] = 1;

  for (i = 1; i <= nbytes; i++) {
    zero_poly(tp);
    tp[0] = gexp[i];		/* x+a^n */
    tp[1] = 1;

    mult_polys(genpoly, tp, tp1);
    copy_poly(tp1, genpoly);
  }
}

/*
 * биты четности идут в pByets, все сообщение
 * и четность копируются в dest для создания кодового слова
 *
 */

void
encode_data (unsigned char msg[], int nbytes, unsigned char dst[])
{
  int i, LFSR[NPAR+1],dbyte, j;

  for(i=0; i < NPAR+1; i++) LFSR[i]=0;

  for (i = 0; i < nbytes; i++) {
    dbyte = msg[i] ^ LFSR[NPAR-1];
    for (j = NPAR-1; j > 0; j--) {
      LFSR[j] = LFSR[j-1] ^ gmult(genPoly[j], dbyte);
    }
    LFSR[0] = gmult(genPoly[0], dbyte);
  }

  for (i = 0; i < NPAR; i++)
    pBytes[i] = LFSR[i];

  build_codeword(msg, nbytes, dst);
}

